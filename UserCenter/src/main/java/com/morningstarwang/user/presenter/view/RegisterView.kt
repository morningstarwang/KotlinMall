package com.morningstarwang.user.presenter.view

import com.morningstarwang.base.presenter.view.BaseView

interface RegisterView: BaseView{
    fun onRegisterResult(result: Boolean)
}