package com.morningstarwang.user.presenter

import com.morningstarwang.base.ext.execute
import com.morningstarwang.base.presenter.BasePresenter
import com.morningstarwang.base.rx.BaseSubscriber
import com.morningstarwang.user.presenter.view.RegisterView
import com.morningstarwang.user.service.impl.UserServiceImpl
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class RegisterPresenter: BasePresenter<RegisterView>() {
    fun register(mobile: String, verifyCode: String, password: String){
        /*
        业务逻辑
         */
        val userService = UserServiceImpl()
        userService.register(mobile, verifyCode, password)
                .execute(object: BaseSubscriber<Boolean>(){
                    override fun onNext(t: Boolean) {
                        mView.onRegisterResult(t)
                    }
                })
    }
}