package com.morningstarwang.user.data.repository

import com.morningstarwang.base.data.net.RetrofitFactory
import com.morningstarwang.base.data.protocol.BaseResp
import com.morningstarwang.user.data.api.UserApi
import com.morningstarwang.user.data.protocol.RegisterReq
import rx.Observable

class UserRepository {
    fun register(mobile: String, pwd: String, verifyCode: String ): Observable<BaseResp<String>>{
        return RetrofitFactory.instance.create(UserApi::class.java)
                .register(RegisterReq(mobile, pwd, verifyCode))
    }
}