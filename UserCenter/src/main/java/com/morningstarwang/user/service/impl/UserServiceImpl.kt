package com.morningstarwang.user.service.impl

import com.morningstarwang.base.data.protocol.BaseResp
import com.morningstarwang.base.rx.BaseException
import com.morningstarwang.user.data.repository.UserRepository
import com.morningstarwang.user.service.UserService
import rx.Observable
import rx.functions.Func1

class UserServiceImpl: UserService {
    override fun register(mobile: String, pwd: String, verifyCode: String): Observable<Boolean> {
        val repository = UserRepository()
        return repository.register(mobile, pwd, verifyCode)
                .flatMap(object :Func1<BaseResp<String>, Observable<Boolean>>{
                    override fun call(t: BaseResp<String>): Observable<Boolean> {
                        if(t.status != 0){
                            return Observable.error(BaseException(t.status, t.message))
                        }
                        return Observable.just(true)
                    }

                })
    }

}