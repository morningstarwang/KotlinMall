package com.morningstarwang.base.rx

class BaseException(val status:Int, val msg: String): Throwable(){}