package com.morningstarwang.base.ui.activity

import com.morningstarwang.base.presenter.BasePresenter
import com.morningstarwang.base.presenter.view.BaseView

open class BaseMvpActivity<T:BasePresenter<*>> : BaseActivity(), BaseView {
    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun onError() {
    }

    lateinit var mPresenter:T
}