package com.morningstarwang.base.presenter

import com.morningstarwang.base.presenter.view.BaseView

open class BasePresenter<T:BaseView>{
    lateinit var mView: T
}